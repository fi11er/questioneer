from django import template
from questioneer.forms import *
from django.db.models import Count
from questioneer.models import *

register = template.Library()

@register.inclusion_tag('questioneer/tags/tags_cloud.html')
def tagscloud():
	tags_query = Tag.objects.annotate(questions_num=Count('question')).order_by('-questions_num')[:20]
	coef = tags_query[0].questions_num - tags_query[len(tags_query) - 1].questions_num
	for k in tags_query:
		k.questions_num = k.questions_num * 4 / coef + 1
	return {'tags': sorted(tags_query, key=lambda o: o.name)}

@register.inclusion_tag('questioneer/tags/login.html')
def login(user):
	login = LoginForm(prefix='login')
	signup = SignupForm(prefix='signup')
	return {'user': user, 'login': login, 'signup': signup}
	
