function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function like(id) {
	data = {
		id: id, 
		type: 'like',
		csrfmiddlewaretoken: getCookie('csrftoken')
	};
	$.post("/ajax/like/", data, onSuccessLike);
}

function onSuccessLike(data) {
	if (data.status == "ok"){
		$('#like'+data.id+' i').addClass('icon-star');
		$('#like'+data.id+' i').removeClass('icon-star-empty');
		$('#like'+data.id+' span').text(data.likes);
		$('#like'+data.id+' a').attr('onclick','unlike('+data.id+')');
	}
}

function unlike(id) {
	data = {
		id: id, 
		type: 'unlike',
		csrfmiddlewaretoken: getCookie('csrftoken')
	};
	$.post("/ajax/like/", data, onSuccessUnlike);
}

function onSuccessUnlike(data) {
	if (data.status == "ok"){
		$('#like'+data.id+' i').addClass('icon-star-empty');
		$('#like'+data.id+' i').removeClass('icon-star');
		$('#like'+data.id+' span').text(data.likes);
		$('#like'+data.id+' a').attr('onclick','like('+data.id+')');
	}
}

function usefull(id) {
	data = {
		id: id,
		csrfmiddlewaretoken: getCookie('csrftoken')
	};
	$.post("/ajax/usefull/", data, onSuccessUsefull);
}

function onSuccessUsefull(data) {
	if (data.status == "ok"){
		$('#answer'+data.id+' h4.media-heading').append('<span class="label label-success">Полезный ответ</span>')
		$('#answer'+data.id+' .usefull-btn').remove()
	}
}

function vote(id, type, val) {
	data = {
		id: id,
		type: type,
		val: val,
		csrfmiddlewaretoken: getCookie('csrftoken')
	};
	$.post("/ajax/vote/", data, onSuccessVote);
}

function onSuccessVote(data) {
	if (data.status == "ok"){
		$('#'+data.type+data.id+' span.vote').text(data.vote)
	}
}