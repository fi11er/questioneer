# -*- coding: utf-8 -*-
from django import forms

class LoginForm(forms.Form):
	login = forms.CharField(
			label='Имя пользователя',
			widget=forms.TextInput(attrs={
				'class': 'form-control',
				'placeholder': 'Имя пользователя',
				'required': 'required',
				'autofocus': 'autofocus'}))
	password = forms.CharField(label='Пароль', widget=forms.PasswordInput(attrs={
				'class': 'form-control',
				'placeholder': 'Пароль',
				'required': 'required',
				'autofocus': 'autofocus'}))

class SignupForm(forms.Form):
	email = forms.EmailField(label='Электронная почта')
	login = forms.CharField(label='Имя пользователя')
	password = forms.CharField(label='Пароль', widget=forms.PasswordInput)
	password_confirmation = forms.CharField(label='Подтверждение пароля', widget=forms.PasswordInput)

class QuestionForm(forms.Form):
	title = forms.CharField(label='Заголовок', widget=forms.TextInput(attrs={'class': 'span12'}))
	text = forms.CharField(label='Подробности', widget=forms.Textarea(attrs={'class': 'span12 resize-vertical', 'rows': 3}))
	tags = forms.CharField(label='Теги', widget=forms.TextInput(attrs={'class': 'span12'}))

class AnswerForm(forms.Form):
	title = forms.CharField(label='Заголовок', widget=forms.TextInput(attrs={'class': 'span12'}))
	text = forms.CharField(label='Подробности', widget=forms.Textarea(attrs={'class': 'span12 resize-vertical', 'rows': 3}))