from django.conf.urls import patterns, url

from questioneer import views

urlpatterns = patterns('', 
		url(r'^$', views.new_questions, name='new_questions'),
		url(r'^questions/popular/$', views.popular_questions, name='popular_questions'),
		url(r'^questions/noanswer/$', views.unanswered_questions, name='unanswered_questions'),
		url(r'^q/(?P<q_id>\d+)/$', views.question, name='question'),
		url(r'^questions/add/$', views.add_question, name='add_question'),
		url(r'^q/(?P<q_id>\d+)/answer/$', views.add_answer, name='add_answer'),

		url(r'^ajax/like/$', views.like, name='like'),
		url(r'^ajax/usefull/$', views.usefull, name='usefull'),
		url(r'^ajax/vote/$', views.vote, name='vote'),


		url(r'^tags/$', views.tags, name='tags'),
		url(r'^tag/(?P<t_id>\d+)/$', views.tag, name='tag'),

		url(r'^users/$', views.users, name='users'),
		url(r'^user/(?P<u_id>\d+)/$', views.user_info, name='user'),
		url(r'^user/(?P<u_id>\d+)/questions/$', views.user_questions, name='user_questions'),
		url(r'^user/(?P<u_id>\d+)/answers/$', views.user_answers, name='user_answers'),
		url(r'^signup/$', views.signup, name='signup'),
		url(r'^login/$', views.login, name='login'),
		url(r'^logout/$', views.logout, name='logout'),
)