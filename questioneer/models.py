from django.db import models
from django.contrib.contenttypes.generic import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from datetime import datetime
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
	avatar = models.ImageField(upload_to = "avatars")

	def __unicode__(self):
		return super(User, self).__unicode__()


class Tag(models.Model):
	name = models.CharField(max_length = 100)

	def __unicode__(self):
		return self.name


class Question(models.Model):
	title = models.CharField(max_length = 200)
	text = models.TextField()
	user = models.ForeignKey(User)
	date = models.DateTimeField(default = datetime.now())
	views = models.IntegerField(default = 0)
	tags = models.ManyToManyField(Tag)
	likes = models.ManyToManyField(User, through="Like", related_name='likes')

	def __unicode__(self):
		return self.title


class Answer(models.Model):
	title = models.CharField(max_length = 200)
	question = models.ForeignKey(Question)
	text = models.TextField()
	user = models.ForeignKey(User)
	date = models.DateTimeField(default = datetime.now())
	usefull = models.BooleanField(default = False)

	def __unicode__(self):
		return self.title


class Vote(models.Model):
	value = models.SmallIntegerField()
	user = models.ForeignKey(User)
	content_type = models.ForeignKey(ContentType)
	object_id = models.PositiveIntegerField()
	content_object = GenericForeignKey('content_type', 'object_id')


class Like(models.Model):
	user = models.ForeignKey(User)
	question = models.ForeignKey(Question)
	 