from django.http import Http404, HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.contrib import auth
from django.db.models import Count, Sum
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from questioneer.forms import *
from questioneer.models import *
from django.core.urlresolvers import reverse
import smtplib
import json

def get_questions():
	ctype = ContentType.objects.get_for_model(Question)
	return Question.objects.annotate(
		answers_num=Count('answer', distinct=True), like_num=Count(
		'like', distinct=True)).extra(
		select={'usefull': """
		SELECT count(*) FROM questioneer_answer 
		WHERE question_id = questioneer_question.id 
			AND usefull = 1""", 'votes': """
		SELECT ifnull(sum(questioneer_vote.value), 0) FROM questioneer_vote 
		WHERE questioneer_vote.content_type_id=%s 
		AND questioneer_vote.object_id = questioneer_question.id"""},
		select_params=[ctype.pk]).prefetch_related('tags', 'likes').select_related('user')

def new_questions(request):
	question_list = get_questions().order_by('-date')
	paginator = Paginator(question_list, 10)
	page = request.GET.get('page')
	try:
		questions_page = paginator.page(page)
	except PageNotAnInteger:
		questions_page = paginator.page(1)
	except EmptyPage:
		questions_page = paginator.page(paginator.num_pages)

	form = QuestionForm()
	context = {
		'question_page': questions_page,
		'active_tab': 'new',
		'active_page': 'questions',
		'form': form,
	}
	return render(request, 'questioneer/question_list.html', context)


def popular_questions(request):
	question_list = get_questions().order_by('-views')

	paginator = Paginator(question_list, 10)
	page = request.GET.get('page')
	try:
		questions_page = paginator.page(page)
	except PageNotAnInteger:
		questions_page = paginator.page(1)
	except EmptyPage:
		questions_page = paginator.page(paginator.num_pages)

	form = QuestionForm()
	context = {
		'question_page': questions_page,
		'active_tab': 'popular',
		'active_page': 'questions',
		'form': form,
	}
	return render(request, 'questioneer/question_list.html', context)


def unanswered_questions(request):
	question_list = get_questions().filter(answers_num=0).order_by('-date')

	paginator = Paginator(question_list, 10)
	page = request.GET.get('page')
	try:
		questions_page = paginator.page(page)
	except PageNotAnInteger:
		questions_page = paginator.page(1)
	except EmptyPage:
		questions_page = paginator.page(paginator.num_pages)

	form = QuestionForm()
	context = {
		'question_page': questions_page,
		'active_tab': 'noanswer',
		'active_page': 'questions',
		'form': form,
	}
	return render(request, 'questioneer/question_list.html', context)


def question(request, q_id):
	try:
		qtype = ContentType.objects.get_for_model(Question)
		question = Question.objects.extra(
		select={'votes': """
		SELECT ifnull(sum(value), 0) FROM questioneer_vote 
		WHERE content_type_id=%s 
		AND object_id = questioneer_question.id"""},
		select_params=[qtype.pk]).annotate(like_num=Count('like', distinct=True)).prefetch_related('tags').select_related('answer_set__user').get(pk = q_id)
	except Question.DoesNotExist:
		raise Http404
	atype = ContentType.objects.get_for_model(Answer)
	answers = Answer.objects.filter(question=question).extra(
		select={'vote': """
		SELECT ifnull(sum(value), 0) FROM questioneer_vote 
		WHERE content_type_id=%s 
		AND id = questioneer_answer.id"""},
		select_params=[atype.pk]).select_related('user', 'question').all()
	question.views += 1
	question.save()
	form = AnswerForm()
	context = {
		'question': question,
		'answers': answers,
		'form': form,
	}
	return render(request, 'questioneer/question_page.html', context)


def add_question(request):
	if request.method == 'POST':
		form = QuestionForm(request.POST)
		if form.is_valid():
			title = form.cleaned_data['title']
			text = form.cleaned_data['text']
			tags = form.cleaned_data['tags']
			q = Question.objects.create(title=title, text=text, user=request.user)
			tag_list = [Tag(name=tag.strip()) for tag in ' '.join(tags.split()).split(',') if tag.strip() != '']
			tag_objects = [Tag.objects.get_or_create(name=tag)[0] for tag in tag_list]
			q.tags = tag_objects
			return HttpResponseRedirect(reverse('question', args=(q.id,)))
		else:
			pass
	return HttpResponseRedirect('/')


def add_answer(request, q_id):
	try:
		question = Question.objects.get(pk = q_id)
	except Question.DoesNotExist:
		raise Http404
	if request.method == 'POST':
		form = AnswerForm(request.POST)
		if form.is_valid():
			title = form.cleaned_data['title']
			text = form.cleaned_data['text']
			question.answer_set.create(title=title, text=text, user=request.user)
			s = smtplib.SMTP('smtp.gmail.com:587')
			s.starttls()
			s.login('ovloleg@gmail.com', '19921992')
			s.sendmail('info@questioneer.ru', [question.user.email], 'New answer added')
			return HttpResponseRedirect(reverse('question', args=(question.id,)))
		else:
			pass
	return HttpResponseRedirect('/')


def tags(request):
	tag_list = Tag.objects.annotate(questions_num=Count('question', distinct=True)).order_by('-questions_num')

	paginator = Paginator(tag_list, 10)
	page = request.GET.get('page')
	try:
		tag_page = paginator.page(page)
	except PageNotAnInteger:
		tag_page = paginator.page(1)
	except EmptyPage:
		tag_page = paginator.page(paginator.num_pages)

	context = {
		'tag_page': tag_page,
		'active_page': 'tags'
	}
	return render(request, 'questioneer/tag_list.html', context)


def tag(request, t_id):
	try:
		tag = Tag.objects.annotate(questions_num=Count('question', distinct=True)).get(pk = t_id)
	except Tag.DoesNotExist:
		raise Http404
	questions = get_questions().filter(tags__in=[tag]).order_by('-date')
	paginator = Paginator(questions, 10)
	page = request.GET.get('page')
	try:
		question_page = paginator.page(page)
	except PageNotAnInteger:
		question_page = paginator.page(1)
	except EmptyPage:
		question_page = paginator.page(paginator.num_pages)

	context = {
		'tag': tag,
		'question_page': question_page
	}
	return render(request, 'questioneer/tag_page.html', context)



def users(request):
	user_list = User.objects.annotate(questions_num=Count('question', distinct=True), answers_num=Count('answer', distinct=True)).order_by('username')

	paginator = Paginator(user_list, 10)
	page = request.GET.get('page')
	try:
		users_page = paginator.page(page)
	except PageNotAnInteger:
		users_page = paginator.page(1)
	except EmptyPage:
		users_page = paginator.page(paginator.num_pages)

	context = {
		'user_page': users_page,
		'active_page': 'users'
	}
	return render(request, 'questioneer/user_list.html', context)


def user_info(request, u_id):
	try:
		user = User.objects.annotate(questions_num=Count('question', distinct=True), answers_num=Count('answer', distinct=True)).get(pk=u_id)
	except User.DoesNotExist:
		raise Http404
	context = {
		'user_profile': user,
		'active_tab': 'info',
	}
	return render(request, 'questioneer/user_info.html', context)


def user_questions(request, u_id):
	try:
		user = User.objects.annotate(questions_num=Count('question', distinct=True), answers_num=Count('answer', distinct=True)).get(pk=u_id)
	except User.DoesNotExist:
		raise Http404

	question_list = get_questions().filter(user=user).order_by('-date')

	paginator = Paginator(question_list, 10)
	page = request.GET.get('page')
	try:
		questions_page = paginator.page(page)
	except PageNotAnInteger:
		questions_page = paginator.page(1)
	except EmptyPage:
		questions_page = paginator.page(paginator.num_pages)
	context = {
		'user_profile': user,
		'active_tab': 'questions',
		'question_page': questions_page
	}
	return render(request, 'questioneer/user_questions.html', context)


def user_answers(request, u_id):
	try:
		user = User.objects.annotate(questions_num=Count('question', distinct=True), answers_num=Count('answer', distinct=True)).get(pk=u_id)
	except User.DoesNotExist:
		raise Http404

	atype = ContentType.objects.get_for_model(Answer)
	answer_list = Answer.objects.filter(user=user).extra(
		select={'vote': """
		SELECT ifnull(sum(value), 0) FROM questioneer_vote 
		WHERE content_type_id=%s 
		AND object_id = questioneer_answer.id"""},
		select_params=[atype.pk]).order_by('-date')

	paginator = Paginator(answer_list, 10)
	page = request.GET.get('page')
	try:
		answer_page = paginator.page(page)
	except PageNotAnInteger:
		answer_page = paginator.page(1)
	except EmptyPage:
		answer_page = paginator.page(paginator.num_pages)
	context = {
		'user_profile': user,
		'active_tab': 'answers',
		'answer_page': answer_page
	}
	return render(request, 'questioneer/user_answers.html', context)


def signup(request):
	if request.method == 'POST':
		form = SignupForm(request.POST, prefix='signup')
		if form.is_valid():
			username = form.cleaned_data['login']
			email = form.cleaned_data['email']
			password = form.cleaned_data['password']
			password_confirmation = form.cleaned_data['password_confirmation']
			User.objects.create_user(username, email, password)
			user = auth.authenticate(username=username, password=password)
			if user is not None:
				auth.login(request, user)
			else:
				return HttpResponseRedirect('/q/1')
		else:
			return HttpResponseRedirect('/q/2')
	return HttpResponseRedirect(request.META['HTTP_REFERER'])


def login(request):
	if request.method == 'POST':
		form = LoginForm(request.POST, prefix='login')
		if form.is_valid():
			username = form.cleaned_data['login']
			password = form.cleaned_data['password']
			user = auth.authenticate(username=username, password=password)
			if user is not None:
				if user.is_active:
					auth.login(request, user)
				else:
					pass
			else:
				pass
		else:
			pass
	return HttpResponseRedirect(request.META['HTTP_REFERER'])


def logout(request):
	auth.logout(request)
	return HttpResponseRedirect(request.META['HTTP_REFERER'])

def like(request):
	if request.method != 'POST' or not request.user.is_authenticated():
		data = {'status': 'error'}
	else:
		q_id = request.POST.get('id')
		like_type = request.POST.get('type')
		try:
			question = Question.objects.get(pk=q_id)
			if not Like.objects.filter(user=request.user, question=question).exists() and like_type == 'like':
				Like.objects.create(user=request.user, question=question)
			elif Like.objects.filter(user=request.user, question=question).exists() and like_type == 'unlike':
				Like.objects.filter(user=request.user, question=question).delete()
			else:
				raise Exception
			likes = Like.objects.filter(question=question).count()
			data = {'status': 'ok', 'likes': likes, 'id': q_id}
		except(Question.DoesNotExist, ValueError, Exception):
			data = {'status': 'error'}
	return HttpResponse(json.dumps(data), content_type = 'application/json')

def usefull(request):
	if request.method != 'POST' or not request.user.is_authenticated():
		data = {'status': 'error'}
	else:
		id = request.POST.get('id')
		try:
			answer = Answer.objects.get(pk=id)
			if answer.usefull == 0 and answer.question.user == request.user:
				answer.usefull=1
				answer.save()
				data = {'status': 'ok', 'id': id}
			else:
				data = {'status': 'error'}
		except(Answer.DoesNotExist, ValueError):
			data = {'status': 'error'}
	return HttpResponse(json.dumps(data), content_type = 'application/json')

def vote(request):
	if request.method != 'POST' or not request.user.is_authenticated():
		data = {'status': 'error'}
	else:
		id = request.POST.get("id")
		type = request.POST.get("type")
		val = request.POST.get("val")

		try:
			if type == "answer":
				object = Answer.objects.get(pk = int(id))
			elif type == "question":
				object = Question.objects.get(pk = int(id))
			else:
				raise Exception
			ctype = ContentType.objects.get_for_model(object)
			if Vote.objects.filter(content_type = ctype, object_id = object.id, user = request.user).exists():
				Vote.objects.filter(content_type = ctype, object_id = object.id, user = request.user).update(value=int(val))
			else:	
				Vote.objects.create(value=int(val), user = request.user, content_object = object)

			cur_vote = Vote.objects.filter(content_type = ctype, object_id = object.id).aggregate(vote_sum=Sum('value'))['vote_sum']
			data = {"id": id, "type": type, 'vote': cur_vote, "status": "ok"} 
		except (Answer.DoesNotExist, Question.DoesNotExist, ValueError, Exception):
			data = {"status": "error"}
	return HttpResponse(json.dumps(data), content_type = 'application/json')